# ml-webscrapping

Realiza uma pesquisa no Mercado livre utilizando Python e armazena os dados em um arquivo .csv na pasta [dados](dados)


---

## Como utilizar o pacote

Tenha instalado o Webdriver do Firefox (geckodriver) e tenha ele nas variáveis do
sistema

Execute no terminal os comandos
```shell
pip install -r requirements.txt
```

Altere os parâmetros do arquivo [config.yml](config.yml), onde
- `busca` -> é o termo que será utilizado na pesquisa do mercado livre
- `numero_paginas` -> o limite do número de páginas que deseja-se pesquisar
- `tempo_espera` -> *delay* em segundos após abrir cada página, em conexões mais lentas aumente este número

após configurado corretamente simplesmente execute o arquivo [src/main.py](src/main.py).

```shell
python main.py
```
---

## Sobre o projeto

Na disciplina de Computação em estatística 2 em Python, 
tivemos um exercício de realizar o webscrapping dos produtos do mercado livre utilizando Python.
Naquele exercício utilizei os pacotes `requests` e `bs4`. Em 2022 o script estava desatualizado.
E não consegui realizar um novo *webscrapping* utilizando apenas comandos de `GET`e `POST` como já havia sido feito.

Visando em ampliar o conhecimento em Python, utilizei o pacote `selenium`
com o webdriver do Firefox para fazer uma pesquisa e salvar os dados em um
arquivo `.csv`.