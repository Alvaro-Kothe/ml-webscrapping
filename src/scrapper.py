from dataclasses import dataclass
from typing import List
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.common.by import By
import pandas as pd

def _remove_br_format(preco: str) -> float:
    """Remove os pontos como separador de milhar e transforma a virgula como um ponto para separador de decimal

    Args:
        preco (str): preco como valor numerico

    Returns:
        float: preco como ponto flutuante
    """
    preco_numerico = preco.replace('.', '').replace(',', '.')
    return float(preco_numerico)


@dataclass
class Anuncio:
    """Armazena os dados principais do anuncio
    """
    nome: str
    preco: float
    url: str


class ParserML:

    def __init__(self):
        self.produtos: List[Anuncio] = []

    def _le_anuncio(self, item_anuncio: WebElement) -> Anuncio:
        nome = item_anuncio.find_element(By.TAG_NAME, 'h2')
        url_produto = item_anuncio.find_element(
            By.TAG_NAME, 'a').get_attribute('href')
        preco_produto = item_anuncio.find_element(
            By.CLASS_NAME, 'price-tag-fraction')

        return Anuncio(nome.text, _remove_br_format(preco_produto.text), url_produto)

    def parse_products(self, anuncios: List[WebElement]) -> List[Anuncio]:
        anuncios = [self._le_anuncio(anuncio) for anuncio in anuncios]
        self.produtos += anuncios
    
    def to_csv(self, filepath: str):
        df = pd.DataFrame(self.produtos)
        df.to_csv(filepath, index=False)