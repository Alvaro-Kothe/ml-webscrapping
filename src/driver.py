from abc import ABC, abstractmethod
from typing import List
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement


class Buscador(ABC):
    @abstractmethod
    def set_up(self):
        pass

    @abstractmethod
    def busca(self, query: str):
        pass

    @abstractmethod
    def close(self):
        pass


class BuscaMercadoLivre(Buscador):
    def set_up(self, headless=True):
        firefox_options = webdriver.FirefoxOptions()
        if headless:
            firefox_options.add_argument('--headless')
        self.driver = webdriver.Firefox(options=firefox_options)
        self.go2pagina_principal()

    def go2pagina_principal(self):
        url = 'https://www.mercadolivre.com.br/'
        self.driver.get(url)

    def busca(self, query: str):
        assert 'mercadolivre' in self.driver.current_url
        search_form = self.get_search_form()
        search_form.send_keys(query)
        search_form.submit()

    def get_search_form(self):
        search_form = self.driver.find_element(By.ID, 'cb1-edit')
        return search_form

    def get_page_products(self) -> List[WebElement]:
        return self.driver.find_elements(By.CLASS_NAME, "ui-search-result__content-wrapper")

    def go2_next_page(self):
        botoes_navegacao = self.driver.find_element(
            By.CLASS_NAME, 'ui-search-pagination')
        seguinte = botoes_navegacao.find_element(By.LINK_TEXT, 'Seguinte')
        self.driver.get(seguinte.get_attribute('href'))

    def close(self):
        self.driver.close()
