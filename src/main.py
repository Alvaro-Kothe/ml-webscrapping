from datetime import date
import time
from driver import BuscaMercadoLivre
import yaml
from scrapper import ParserML
from utils import slugify


def main():
    with open("config.yml", "r") as ymlfile:
        cfg = yaml.safe_load(ymlfile)

    buscador = BuscaMercadoLivre()
    parser = ParserML()
    buscador.set_up(headless=False)
    time.sleep(cfg['tempo_espera'])
    buscador.busca(cfg['busca'])
    try:
        while cfg['numero_paginas'] > 0:
            time.sleep(cfg['tempo_espera'])
            parser.parse_products(buscador.get_page_products())
            if cfg['numero_paginas'] > 1:
                buscador.go2_next_page()
            cfg['numero_paginas'] -= 1
    finally:
        buscador.close()
    
    today = date.today().isoformat()
    search_slug = slugify(cfg['busca'])
    parser.to_csv(f'data/{today}-{search_slug}.csv')


if __name__ == '__main__':
    main()
